#ifndef LEVEL_H
#define LEVEL_H

#include <deque>
#include <list>
#include <vector>
#include <SDL.h>
#include "Ball.h"
#include "Gun.h"

namespace Game {

enum {
	CELL_EMPTY = 0,
	CELL_BUSY = 1,
	CELL_GUN = 2,
	CELL_START = 3,
	CELL_WIN = 4,
	CELL_PATROL = 5,
	MAP_ROWS = 31,
	MAP_COLS = 31
};

static const char map[MAP_ROWS][MAP_COLS] = {
	{2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 2},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 4, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 2},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

/* Level */

class Level {
public:
	Level(const char* map, int rows, int cols, int width, int height);

	~Level();

	bool IsFinished() const;

	void Reset(const char* map);

	void MouseLeftUp(int x, int y);

	void MouseRightUp(int x, int y);

	void Update(float deltaTime);

	void Draw(SDL_Renderer* renderer) const;

private:
	enum {
		CELL_WIDTH = 48,
		CELL_HEIGHT = CELL_WIDTH,
		TILE_WIDTH = CELL_WIDTH,
		TILE_HEIGHT = CELL_WIDTH / 2,
		BALL_RADIUS = CELL_WIDTH / 2 - 2,
		LEE_EMPTY = -2,
		LEE_BUSY = -1,
		GUN_MIN_DELAY = 2,
		GUN_MAX_DELAY = 4,
		GUN_MIN_DURATION = 3,
		GUN_MAX_DURATION = 5
	};

	struct RowCol {
		int row;
		int col;
		RowCol(int row, int col) : row(row), col(col) {}
	};

	typedef std::vector<Gun*> Guns;
	typedef std::vector<std::vector<char>> Grid;
	typedef std::vector<std::vector<short>> LeeGrid;
	typedef std::vector<RowCol> PathPlayer;
	typedef std::deque<RowCol> PathPatrol;
	typedef std::vector<RowCol> StackPathPatrol;
	typedef std::list<Ball> Bullets;

private:
	void DeleteGuns();

	bool CheckRange(int row, int col) const;

	int GetCenterX(int col) const;

	int GetCenterY(int row) const;

	int GetTileX(int row, int col) const;

	int GetTileY(int row, int col) const;

	int GetRow(int y) const;

	int GetRow(int x, int y) const;

	int GetCol(int x) const;

	int GetCol(int x, int y) const;

	void FindPath(RowCol beg, RowCol end);

	void ScreenToIsometricCoords(int& x, int& y) const;

	bool HasPlayer(int row, int col) const;

	bool HasPath(int row, int col) const;

	static int RandInt(int min, int max);

	static void DrawTile(SDL_Renderer* renderer, Sint16 x, Sint16 y, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);

	static void DrawBottomWall(SDL_Renderer* renderer, Sint16 x, Sint16 y, Uint8 red, Uint8 green, Uint8 blue);

	static void DrawRightWall(SDL_Renderer* renderer, Sint16 x, Sint16 y, Uint8 red, Uint8 green, Uint8 blue);

private:
	const int mRows;
	const int mCols;
	const int mOffsetX;
	const int mOffsetY;
	Grid mGrid;
	LeeGrid mLeeGrid;
	bool mIsFinished;
	Ball mPlayer;
	PathPlayer mPathPlayer;
	Ball mPatrol;
	PathPatrol mPathPatrol;
	StackPathPatrol mStackPathPatrol;
	Guns mBottomGuns;
	Guns mRightGuns;
	Bullets mBullets;
};

} /* namespace Game */

#endif /* LEVEL_H */
