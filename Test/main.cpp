#include <ctime>
#include <SDL.h>
#include "Level.h"

using namespace Game;

enum {
	WIDTH = 1600,
	HEIGHT = 1024
};

int main(int argc, char* argv[]) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		return 1;
	}

	SDL_Window* window = SDL_CreateWindow("Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	if (window == NULL) {
		SDL_Quit();
		return 1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL) {
		SDL_DestroyWindow(window);
		SDL_Quit();
		return 1;
	}


	clock_t ticks = 0;
	Level level(&map[0][0], MAP_ROWS, MAP_COLS, WIDTH, HEIGHT);
	SDL_Event event;
	while (true) {
		if (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				break;
			} else if (event.type == SDL_MOUSEBUTTONUP) {
				if (event.button.button == SDL_BUTTON_LEFT) {
					level.MouseLeftUp(event.button.x, event.button.y);
				}
				else if (event.button.button == SDL_BUTTON_RIGHT) {
					level.MouseRightUp(event.button.x, event.button.y);
				}
			}
		}

		const clock_t now = clock();
		float deltaTime = static_cast<float>((now - ticks)) / CLOCKS_PER_SEC;
		ticks = now;
		if (deltaTime > 0.2) {
			deltaTime = 0.2f;
		}

		level.Update(deltaTime);
		if (level.IsFinished()) {
			level.Reset(&map[0][0]);
		}

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);
		level.Draw(renderer);
		SDL_RenderPresent(renderer);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
