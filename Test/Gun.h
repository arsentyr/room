#ifndef GUN_H
#define GUN_H

#include <cassert>

namespace Game {

/* Gun */

class Gun {
public:
	Gun(int delay, int duration)
			: mDelay(delay)
			, mDuration(static_cast<float>(duration))
			, mTime(static_cast<float>(delay)) {
		assert(delay > 0 && duration > 0);
	}

	void Start() {
		mTime = static_cast<float>(mDelay);
	}

	bool Update(float deltaTime) {
		if (IsFinished()) {
			return false;
		}
		mTime -= deltaTime;
		return true;
	}

	float Duration() const {
		return mDuration;
	}

	bool IsFinished() const {
		return mTime <= 0;
	}

private:
	const int mDelay;
	const float mDuration;
	float mTime;
};

} /* namespace Game */

#endif /* GUN_H */
