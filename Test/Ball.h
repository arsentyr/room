#ifndef BALL_H
#define BALL_H

#include <cassert>
#include <limits>

namespace Game {

/* Tween */

template<typename T>
class Tween {
public:
	explicit Tween(const T& initial = T(), const T& end = T(), float duration = std::numeric_limits<float>::min())
			: mValue(initial)
			, mState(STATE_PAUSED) {
		SetInitial(initial);
		SetEnd(end);
		SetDuration(duration);
		SetTime(0);
		Pause();
	}

	const T& Value() const {
		return mValue;
	}

	void SetInitial(const T& initial) {
		mChange = mChange + mInitial - initial;
		mInitial = initial;
	}

	void SetEnd(const T& end) {
		mChange = end - mInitial;
	}

	T GetEnd() const {
		return mInitial + mChange;
	}

	void SetDuration(float duration) {
		assert(duration > 0);
		mDuration = (duration > 0) ? duration : std::numeric_limits<float>::min();
	}

	void SetTime(float time) {
		assert(time >= 0 && time <= mDuration);
		mTime = (time < 0) ? 0 : ((time > mDuration) ? mDuration : time);
		if (mTime < mDuration && mState == STATE_FINISHED) {
			mState = STATE_PAUSED;
		}
	}

	void Fforward() {
		if (mState != STATE_FINISHED) {
			mTime = mDuration;
			mState = STATE_TWEENING;
			Update(0);
			mState = STATE_FINISHED;
		}
	}

	void Resume() {
		assert(mState != STATE_FINISHED);
		if (mState != STATE_FINISHED) {
			mState = STATE_TWEENING;
			Update(0);
		}
	}

	void Pause() {
		assert(mState != STATE_FINISHED);
		if (mState != STATE_FINISHED) {
			Update(0);
			mState = STATE_PAUSED;
		}
	}

	float Update(float deltaTime) {
		assert(deltaTime >= 0);
		if (mState != STATE_TWEENING) {
			return 0;
		}

		float t = mTime;
		float dt = deltaTime;
		mTime += deltaTime;
		if (mTime < 0) {
			mTime = 0;
			mState = STATE_PAUSED;
			dt = -t;
		}
		else if (mTime >= mDuration) {
			mTime = mDuration;
			mState = STATE_FINISHED;
			if (mTime > mDuration) {
				dt = mDuration - t;
			}
		}
		assert(dt <= deltaTime);

		mValue = mInitial + mChange * (mTime / mDuration);
		return dt;
	}

	bool IsFinished() const {
		return mState == STATE_FINISHED;
	}

private:
	enum TweenState {
		STATE_TWEENING,
		STATE_PAUSED,
		STATE_FINISHED
	};

private:
	T mValue;
	T mInitial;
	T mChange;
	float mDuration;
	float mTime;
	TweenState mState;
};

/* Vector2 */

class Vector2 {
public:
	float x;
	float y;

public:
	Vector2()
			: x(0)
			, y(0) {
	}

	Vector2(float x, float y)
			: x(x)
			, y(y) {
	}

	Vector2(int x, int y)
			: x(static_cast<float>(x))
			, y(static_cast<float>(y)) {
	}

	Vector2 operator+(const Vector2& vec) const {
		return Vector2(x + vec.x, y + vec.y);
	}

	Vector2 operator-(const Vector2& vec) const {
		return Vector2(x - vec.x, y - vec.y);
	}

	Vector2 operator*(float scalar) const {
		return Vector2(x * scalar, y * scalar);
	}

	float Length() const {
		return sqrt(x * x + y * y);
	}
};

/* Ball */

class Ball {
public:
	typedef Tween<Vector2> TweenPosition;

public:
	Ball(int radius)
			: mRadius(radius) {
		assert(radius > 0);
	}

	TweenPosition& Position() {
		return mPosition;
	}

	const TweenPosition& Position() const {
		return mPosition;
	}

	int Radius() const {
		return mRadius;
	}

	int X() const {
		return static_cast<int>(mPosition.Value().x);
	}

	int Y() const {
		return static_cast<int>(mPosition.Value().y);
	}

	int LeftX() const {
		return X() - mRadius;
	}

	int RightX() const {
		return X() + mRadius;
	}

	int BottomY() const {
		return Y() - mRadius;
	}

	int TopY() const {
		return Y() + mRadius;
	}

	bool Hit(const Ball& ball) const {
		return Vector2(mPosition.Value().x - ball.mPosition.Value().x, mPosition.Value().y - ball.mPosition.Value().y).Length() <= mRadius + ball.mRadius;
	}

private:
	TweenPosition mPosition;
	const int mRadius;
};

} /* namespace Game */

#endif /* BALL_H */
