#include "Level.h"

#include <cassert>
#include <cstdlib>
#include <SDL2_gfxPrimitives.h>

namespace Game {

Level::Level(const char* map, int rows, int cols, int width, int height)
		: mRows(rows - 1)
		, mCols(cols - 1)
		, mOffsetX(width / 2)
		, mOffsetY(height / 2 - (mRows + mCols) * TILE_HEIGHT / 4)
		, mIsFinished(false)
		, mPlayer(BALL_RADIUS)
		, mPatrol(BALL_RADIUS){
	assert(map != NULL && rows > 2 && cols > 2 && width > 0 && height > 0);
	// Grid
	mGrid.resize(mRows, std::vector<char>(mCols));
	Reset(map);
	// LeeGrid
	mLeeGrid.resize(mRows + 2, std::vector<short>(mCols + 2));
	mLeeGrid[0].assign(mLeeGrid[0].size(), LEE_BUSY);
	mLeeGrid[mRows + 1].assign(mLeeGrid[mRows + 1].size(), LEE_BUSY);
	for (size_t row = 0; row < mLeeGrid.size(); ++row) {
		mLeeGrid[row][0] = LEE_BUSY;
		mLeeGrid[row][mCols + 1] = LEE_BUSY;
	}
}

Level::~Level() {
	// Guns
	DeleteGuns();
}

bool Level::IsFinished() const {
	return mIsFinished;
}

void Level::Reset(const char* map) {
	assert(map != NULL);
	mIsFinished = false;
	// PathPatrol
	mPathPatrol.clear();
	mStackPathPatrol.clear();
	// Grid
	for (int row = 0; row < mRows; ++row) {
		for (int col = 0; col < mCols; ++col) {
			switch (*(map + (row + 1) * (mCols + 1) + col)) {
			default:
				assert(false);
			case CELL_EMPTY:
				mGrid[row][col] = CELL_EMPTY;
				break;
			case CELL_BUSY:
				mGrid[row][col] = CELL_BUSY;
				break;
			case CELL_START: {
					// Player
					const Vector2 playerPosition(GetCenterX(col), GetCenterY(row)); 
					mPlayer.Position().SetInitial(playerPosition);
					mPlayer.Position().SetEnd(playerPosition);
					mPlayer.Position().SetTime(0);
					mPlayer.Position().Fforward();
				}
				mGrid[row][col] = CELL_EMPTY;
				break;
			case CELL_WIN:
				mGrid[row][col] = CELL_WIN;
				break;
			case CELL_PATROL:
				// PathPatrol
				mPathPatrol.push_back(RowCol(row, col));
				mGrid[row][col] = CELL_PATROL;
				break;
			}
		}
	}
	// PathPlayer
	mPathPlayer.clear();
	// Patrol
	assert(!mPathPatrol.empty());
	const Vector2 patrolPosition(GetCenterX(mPathPatrol.front().col), GetCenterY(mPathPatrol.front().row));
	mPatrol.Position().SetInitial(patrolPosition);
	mPatrol.Position().SetEnd(patrolPosition);
	mPatrol.Position().SetTime(0);
	mPatrol.Position().Fforward();
	// Guns
	DeleteGuns();
	mBottomGuns.resize(mCols, NULL);
	for (size_t col = 0; col < mBottomGuns.size(); ++col) {
		if (*(map + col) == CELL_GUN) {
			mBottomGuns[col] = new Gun(RandInt(GUN_MIN_DELAY, GUN_MAX_DELAY), RandInt(GUN_MIN_DURATION, GUN_MAX_DURATION));
		}
	}
	mRightGuns.resize(mRows, NULL);
	for (size_t row = 0; row < mRightGuns.size(); ++row) {
		if (*(map + (row + 1) * (mCols + 1) + mCols) == CELL_GUN) {
			mRightGuns[row] = new Gun(RandInt(GUN_MIN_DELAY, GUN_MAX_DELAY), RandInt(GUN_MIN_DURATION, GUN_MAX_DURATION));
		}
	}
	// Bullets
	mBullets.clear();
}

void Level::MouseLeftUp(int x, int y) {
	if (IsFinished()) {
		return;
	}
	// PathPlayer
	mPathPlayer.clear();
	// Path
	RowCol beg(GetRow(static_cast<int>(mPlayer.Position().GetEnd().y)), GetCol(static_cast<int>(mPlayer.Position().GetEnd().x)));
	RowCol end(GetRow(x, y), GetCol(x, y));
	if (CheckRange(beg.row, beg.col) && CheckRange(end.row, end.col)
			&& (beg.row != end.row || beg.col != end.col)) {
		FindPath(beg, end);
		if (!mPathPlayer.empty()) {
			mPathPlayer.pop_back();
		}
	}
}

void Level::MouseRightUp(int x, int y) {
	if (IsFinished()) {
		return;
	}
	// Grid
	const int row = GetRow(x, y);
	const int col = GetCol(x, y);
	if (CheckRange(row, col)) {
		switch (mGrid[row][col]) {
		case CELL_EMPTY:
			if (!HasPlayer(row, col) && !HasPath(row, col)) {
				mGrid[row][col] = CELL_BUSY;
			}
			break;
		case CELL_BUSY:
			mGrid[row][col] = CELL_EMPTY;
			break;
		}
	}
}

void Level::Update(float deltaTime) {
	assert(deltaTime >= 0);
	if (IsFinished()) {
		return;
	}
	// Player
	const float playerDeltaTime = mPlayer.Position().Update(deltaTime);
	if (mPlayer.Position().IsFinished()) {
		if (mGrid[GetRow(mPlayer.Y())][GetCol(mPlayer.X())] == CELL_WIN) {
			mIsFinished = true;
		} else if (!mPathPlayer.empty()) {
			RowCol back = mPathPlayer.back();
			mPathPlayer.pop_back();
			mPlayer.Position().SetInitial(Vector2(mPlayer.X(), mPlayer.Y()));
			mPlayer.Position().SetEnd(Vector2(GetCenterX(back.col), GetCenterY(back.row)));
			mPlayer.Position().SetTime(0);
			mPlayer.Position().SetDuration(0.5f);
			mPlayer.Position().Resume();
			mPlayer.Position().Update(deltaTime - playerDeltaTime);
		}
	}
	// Patrol
	const float patrolDeltaTime = mPatrol.Position().Update(deltaTime);
	if (mPatrol.Position().IsFinished()) {
		if (mPathPatrol.empty()) {
			while (!mStackPathPatrol.empty()) {
				mPathPatrol.push_back(mStackPathPatrol.back());
				mStackPathPatrol.pop_back();
			}
		} else {
			RowCol front = mPathPatrol.front();
			mPathPatrol.pop_front();
			mPatrol.Position().SetInitial(Vector2(mPatrol.X(), mPatrol.Y()));
			mPatrol.Position().SetEnd(Vector2(GetCenterX(front.col), GetCenterY(front.row)));
			mPatrol.Position().SetTime(0);
			mPatrol.Position().SetDuration(0.5f);
			mPatrol.Position().Resume();
			mPatrol.Position().Update(deltaTime - patrolDeltaTime);
			mStackPathPatrol.push_back(front);
		}
	}
	// Guns
	for (size_t idx = 0; idx < mBottomGuns.size(); ++idx) {
		if (mBottomGuns[idx] != NULL && !mBottomGuns[idx]->Update(deltaTime)) {
			mBottomGuns[idx]->Start();

			Ball bullet(BALL_RADIUS / 2);
			bullet.Position().SetInitial(Vector2(GetCenterX(-1) + CELL_WIDTH / 2, GetCenterY(idx)));
			bullet.Position().SetEnd(Vector2(GetCenterX(mCols), GetCenterY(idx)));
			bullet.Position().SetTime(0);
			bullet.Position().SetDuration(mBottomGuns[idx]->Duration());
			bullet.Position().Resume();
			mBullets.push_back(bullet);
		}
	}
	for (size_t idx = 0; idx < mRightGuns.size(); ++idx) {
		if (mRightGuns[idx] != NULL && !mRightGuns[idx]->Update(deltaTime)) {
			mRightGuns[idx]->Start();

			Ball bullet(BALL_RADIUS / 2);
			bullet.Position().SetInitial(Vector2(GetCenterX(idx), GetCenterY(-1) + CELL_HEIGHT / 2));
			bullet.Position().SetEnd(Vector2(GetCenterX(idx), GetCenterY(mRows)));
			bullet.Position().SetTime(0);
			bullet.Position().SetDuration(mRightGuns[idx]->Duration());
			bullet.Position().Resume();
			mBullets.push_back(bullet);
		}
	}
	// Bullets
	for (Bullets::iterator it = mBullets.begin(); it != mBullets.end();) {
		it->Position().Update(deltaTime);
		if (it->Position().IsFinished()) {
			it = mBullets.erase(it);
		} else {
			++it;
		}
	}

	if (mPlayer.Hit(mPatrol)) {
		mIsFinished = true;
		return;
	}
	for (Bullets::const_iterator it = mBullets.begin(); it != mBullets.end(); ++it) {
		if (mPlayer.Hit(*it)) {
			mIsFinished = true;
			return;
		}
	}
}

void Level::Draw(SDL_Renderer* renderer) const {
	assert(renderer != NULL);
	// Guns
	for (size_t idx = 0; idx < mBottomGuns.size(); ++idx) {
		const int x = GetTileX(idx - 2, -2);
		const int y = GetTileY(idx - 2, -2);
		if (mBottomGuns[idx] == NULL) {
			DrawBottomWall(renderer, x, y, 206, 216, 216);
		} else {
			DrawBottomWall(renderer, x, y, 255, 0, 0);
		}
	}
	for (size_t idx = 0; idx < mRightGuns.size(); ++idx) {
		const int x = GetTileX(-1, idx);
		const int y = GetTileY(-1, idx) - TILE_HEIGHT - TILE_HEIGHT / 2;
		if (mRightGuns[idx] == NULL) {
			DrawRightWall(renderer, x, y, 206, 216, 216);
		} else {
			DrawRightWall(renderer, x, y, 255, 0, 0);
		}
	}
	// Grid
	for (int row = 0; row < mRows; ++row) {
		for (int col = 0; col < mCols; ++col) {
			const int x = GetTileX(row, col);
			const int y = GetTileY(row, col);
			switch (mGrid[row][col]) {
			case CELL_EMPTY:
			case CELL_PATROL:
				DrawTile(renderer, x, y, 206, 216, 216, 255);
				break;
			case CELL_WIN:
				DrawTile(renderer, x, y, 0, 255, 0, 255);
				break;
			}
		}
	}
	// PathPlayer
	for (size_t idx = 0; idx < mPathPlayer.size(); ++idx) {
		const int x = GetTileX(mPathPlayer[idx].row, mPathPlayer[idx].col);
		const int y = GetTileY(mPathPlayer[idx].row, mPathPlayer[idx].col);
		DrawTile(renderer, x, y, 255, 255, 255, 128);
	}
	if (!mPlayer.Position().IsFinished()) {
		const int row = GetRow(static_cast<int>(mPlayer.Position().GetEnd().y));
		const int col = GetCol(static_cast<int>(mPlayer.Position().GetEnd().x));
		const int x = GetTileX(row, col);
		const int y = GetTileY(row, col);
		DrawTile(renderer, x, y, 255, 255, 255, 128);
	}
	// Bullets
	for (Bullets::const_iterator it = mBullets.begin(); it != mBullets.end(); ++it) {
		int bulletX = it->X() - it->Radius() / 2;
		int bulletY = it->Y() - it->Radius() / 2;
		ScreenToIsometricCoords(bulletX, bulletY);
		filledCircleRGBA(renderer, bulletX, bulletY, it->Radius() / 2, 255, 0, 0, 255);
		circleRGBA(renderer, bulletX, bulletY, it->Radius() / 2, 128, 0, 0, 255);
	}
	// Player
	int playerX = mPlayer.X() - mPlayer.Radius() / 2;
	int playerY = mPlayer.Y() - mPlayer.Radius() / 2;
	ScreenToIsometricCoords(playerX, playerY);
	filledCircleRGBA(renderer, playerX, playerY, mPlayer.Radius() / 2, 0, 255, 0, 255);
	circleRGBA(renderer, playerX, playerY, mPlayer.Radius() / 2, 0, 192, 0, 255);
	// Patrol
	int patrolX = mPatrol.X() - mPatrol.Radius() / 2;
	int patrolY = mPatrol.Y() - mPatrol.Radius() / 2;
	ScreenToIsometricCoords(patrolX, patrolY);
	filledCircleRGBA(renderer, patrolX, patrolY, mPatrol.Radius() / 2, 0, 128, 255, 255);
	circleRGBA(renderer, patrolX, patrolY, mPatrol.Radius() / 2, 0, 0, 255, 255);
}

void Level::DeleteGuns() {
	// Guns
	for (size_t idx = 0; idx < mBottomGuns.size(); ++idx) {
		if (mBottomGuns[idx] != NULL) {
			delete mBottomGuns[idx];
		}
	}
	mBottomGuns.clear();
	for (size_t idx = 0; idx < mRightGuns.size(); ++idx) {
		if (mRightGuns[idx] != NULL) {
			delete mRightGuns[idx];
		}
	}
	mRightGuns.clear();
}

bool Level::CheckRange(int row, int col) const {
	return 0 <= row && row < mRows && 0 <= col && col < mCols;
}

int Level::GetCenterX(int col) const {
	return col * CELL_WIDTH + CELL_WIDTH / 2;
}

int Level::GetCenterY(int row) const {
	return row * CELL_HEIGHT + CELL_HEIGHT / 2;
}

int Level::GetTileX(int row, int col) const {
	return mOffsetX + (col - row - 1) * TILE_WIDTH / 2;
}

int Level::GetTileY(int row, int col) const {
	return mOffsetY + (col + row) * TILE_HEIGHT / 2;
}

int Level::GetRow(int y) const {
	return y / CELL_HEIGHT; 
}

int Level::GetCol(int x) const {
	return x / CELL_WIDTH;
}

int Level::GetRow(int x, int y) const {
	x -= mOffsetX;
	y -= mOffsetY;
	return (y - x / 2) / TILE_HEIGHT;
}

int Level::GetCol(int x, int y) const {
	x -= mOffsetX;
	y -= mOffsetY;
	return (y + x / 2) / TILE_HEIGHT;
}

void Level::FindPath(RowCol beg, RowCol end) {
	assert(CheckRange(beg.row, beg.col) && CheckRange(end.row, end.col));
	for (int row = 0; row < mRows; ++row) {
		for (int col = 0; col < mCols; ++col) {
			mLeeGrid[row + 1][col + 1] = (mGrid[row][col] != CELL_BUSY) ? LEE_EMPTY : LEE_BUSY;
		}
	}
	++beg.row;
	++beg.col;
	++end.row;
	++end.col;
	mLeeGrid[beg.row][beg.col] = 0;
	std::deque<RowCol> queue;
	queue.push_back(RowCol(beg.row, beg.col));
	const int OFFSET_LEN = 8;
	const RowCol offset[OFFSET_LEN] = {
		RowCol(0, -1),
		RowCol(0, 1),
		RowCol(-1, 0),
		RowCol(1, 0),
		RowCol(-1, -1),
		RowCol(1, -1),
		RowCol(-1, 1),
		RowCol(1, 1)
	};
	while (!queue.empty()) {
		RowCol curr = queue.front();
		queue.pop_front();
		const short distance = mLeeGrid[curr.row][curr.col] + 1;
		for (int idx = 0; idx < OFFSET_LEN; ++idx) {
			const int row = curr.row + offset[idx].row;
			const int col = curr.col + offset[idx].col;
			if (mLeeGrid[row][col] == LEE_EMPTY) {
				mLeeGrid[row][col] = distance;
				if (row == end.row && col == end.col) {
					queue.clear();
					break;
				}
				queue.push_back(RowCol(row, col));
			}
		}
	}
	if (mLeeGrid[end.row][end.col] > 0) {
		RowCol curr = end;
		mPathPlayer.push_back(RowCol(curr.row - 1, curr.col - 1));
		while (!(curr.row == beg.row && curr.col == beg.col)) {
			const short distance = mLeeGrid[curr.row][curr.col] - 1;
			for (int idx = 0; idx < OFFSET_LEN; ++idx) {
				const int row = curr.row + offset[idx].row;
				const int col = curr.col + offset[idx].col;
				if (mLeeGrid[row][col] == distance) {
					curr.row = row;
					curr.col = col;
					mPathPlayer.push_back(RowCol(curr.row - 1, curr.col - 1));
					break;
				}
			}
		}
	}
}

void Level::ScreenToIsometricCoords(int& x, int& y) const {
	const int screenX = x;
	const int screenY = y;
	x = (screenX - screenY) / 2 + mOffsetX;
	y = (screenX + screenY) / 4 + mOffsetY;
}

bool Level::HasPlayer(int row, int col) const {
	assert(CheckRange(row, col));
	if ((GetRow(mPlayer.BottomY()) == row || GetRow(mPlayer.TopY()) == row)
			&& GetCol(mPlayer.X()) == col) {
		return true;
	}
	if (GetRow(mPlayer.Y()) == row &&
			(GetCol(mPlayer.LeftX()) == col || GetCol(mPlayer.RightX()) == col)) {
		return true;
	}
	return false;
}

bool Level::HasPath(int row, int col) const {
	for (size_t idx = 0; idx < mPathPlayer.size(); ++idx) {
		if (mPathPlayer[idx].row == row && mPathPlayer[idx].col == col) {
			return true;
		}
	}
	return false;
}

int Level::RandInt(int min, int max) {
    assert(min <= max);
    return rand() % (max - min + 1) + min;
}

void Level::DrawTile(SDL_Renderer* renderer, Sint16 x, Sint16 y, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha) {
	static Sint16 vx[4], vy[4];
	vx[0] = x;
	vy[0] = y + TILE_HEIGHT / 2;
	vx[1] = x + TILE_WIDTH / 2;
	vy[1] = y + TILE_HEIGHT;
	vx[2] = x + TILE_WIDTH;
	vy[2] = vy[0];
	vx[3] = vx[1];
	vy[3] = y;
	filledPolygonRGBA(renderer, vx, vy, 4, red, green, blue, alpha);
	lineRGBA(renderer, vx[0], vy[0], vx[1], vy[1], 255, 255, 255, alpha);
	lineRGBA(renderer, vx[1], vy[1], vx[2], vy[2], 255, 255, 255, alpha);
}

void Level::DrawBottomWall(SDL_Renderer* renderer, Sint16 x, Sint16 y, Uint8 red, Uint8 green, Uint8 blue) {
	static Sint16 vx[4], vy[4];
	vx[0] = x;
	vy[0] = y + TILE_WIDTH + TILE_HEIGHT / 2;
	vx[1] = x + TILE_WIDTH / 2;
	vy[1] = y + TILE_WIDTH;
	vx[2] = vx[1];
	vy[2] = y;
	vx[3] = x;
	vy[3] = y + TILE_HEIGHT / 2;
	filledPolygonRGBA(renderer, vx, vy, 4, red, green, blue, 255);
	polygonRGBA(renderer, vx, vy, 4, 255, 255, 255, 255);
}

void Level::DrawRightWall(SDL_Renderer* renderer, Sint16 x, Sint16 y, Uint8 red, Uint8 green, Uint8 blue) {
	static Sint16 vx[4], vy[4];
	vx[0] = x;
	vy[0] = y + TILE_WIDTH;
	vx[1] = x + TILE_WIDTH / 2;
	vy[1] = y + TILE_WIDTH + TILE_HEIGHT / 2;
	vx[2] = vx[1];
	vy[2] = y + TILE_HEIGHT / 2;
	vx[3] = x;
	vy[3] = y;
	filledPolygonRGBA(renderer, vx, vy, 4, red, green, blue, 255);
	polygonRGBA(renderer, vx, vy, 4, 255, 255, 255, 255);
}

} /* namespace Game */
